package calculations;

public class Factorial {
    static int factorial(int number) {
        int a = 1;
        for (int i = 1; i <= number; i++) {
            a = i * a;
        }
        return a;
    }

    static int factorialByRecursion(int number) {
        if (number == 0) return 1;
        return number * factorialByRecursion(number - 1);
    }
}
